/*
Copyright (C) 2019 Famedly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

import { expect } from "chai";
import * as fs from "fs";
import * as proxyquire from "proxyquire";

// we are a test file and thus our linting rules are slightly different
// tslint:disable:no-unused-expression max-file-line-count no-any

function readJson(path) {
	return JSON.parse(fs.readFileSync(path).toString("utf8"));
}

function getMatrixParser() {
	const bridge = {
		getUrlFromMxc: (s) => s,
	};
	const mp = proxyquire.load("../src/matrixparser", {
		"./util": { Util: {
			DownloadFile: async (url) => {
				return Buffer.alloc(0);
			},
		} },
	});
	return new mp.MatrixParser(bridge as any);
}

describe("MatrixParser", () => {
	describe("parse", () => {
		it("should handle empty collections", async () => {
			const mp = getMatrixParser();
			const parsed = await mp.parse([]);
			expect(parsed).to.be.null;
		});
		it("should handle simple, plain collections", async () => {
			const mp = getMatrixParser();
			const events = readJson("./test/events/plain.json");
			const parsed = await mp.parse(events);
			expect(parsed!.body).to.equal("hello there!\n\n");
			expect(parsed!.attachments.length).to.equal(0);
		});
		it("should handle html collections", async () => {
			const mp = getMatrixParser();
			const events = readJson("./test/events/html.json");
			const parsed = await mp.parse(events);
			expect(parsed!.body).to.equal("**fox**\n\n");
			expect(parsed!.html).to.equal("<strong>fox</strong><br><br>");
			expect(parsed!.attachments.length).to.equal(0);
		});
		it("should handle multipart collections", async () => {
			const mp = getMatrixParser();
			const events = readJson("./test/events/multi.json");
			const parsed = await mp.parse(events);
			expect(parsed!.body).to.equal("roses are red\n\nviolets are blue\n\n");
			expect(parsed!.html).to.equal("roses are red<br><br>violets are blue<br><br>");
			expect(parsed!.attachments.length).to.equal(0);
		});
		it("should handle redactions", async () => {
			const mp = getMatrixParser();
			const events = readJson("./test/events/redact.json");
			const parsed = await mp.parse(events);
			expect(parsed!.body).to.equal("hi\n\n");
			expect(parsed!.html).to.equal("hi<br><br>");
			expect(parsed!.attachments.length).to.equal(0);
		});
		it("should handle edits", async () => {
			const mp = getMatrixParser();
			const events = readJson("./test/events/edit.json");
			const parsed = await mp.parse(events);
			expect(parsed!.body).to.equal("hello cutie\n\n");
			expect(parsed!.html).to.equal("hello cutie<br><br>");
			expect(parsed!.attachments.length).to.equal(0);
		});
		it("should handle images", async () => {
			const mp = getMatrixParser();
			const events = readJson("./test/events/image.json");
			const parsed = await mp.parse(events);
			expect(parsed!.attachments.length).to.equal(1);
			expect(parsed!.attachments[0].filename).to.equal("IMG_20190920_063446.jpg");
			expect(parsed!.attachments[0].contentType).to.equal("image/jpeg");
		});
	});
});
