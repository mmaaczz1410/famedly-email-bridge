FROM node:alpine

COPY . /opt/famedly-email-bridge
WORKDIR /opt/famedly-email-bridge
RUN apk add --no-cache ca-certificates \
	&& apk add --no-cache --virtual .build-deps git make gcc g++ python \
	&& npm install \
	&& npm run build \
	&& apk del .build-deps
VOLUME ["/data"]
ENTRYPOINT ["./docker-run.sh"]
