/*
Copyright (C) 2019 Famedly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

import * as commandLineArgs from "command-line-args";
import * as commandLineUsage from "command-line-usage";
import { Bridge } from "./bridge";
import { Log } from "./log";
import { EmailConfig } from "./config";
import { LogService, IAppserviceRegistration } from "matrix-bot-sdk";
import * as yaml from "js-yaml";
import * as fs from "fs";
import * as uuid from "uuid/v4";

const log = new Log("index");

const commandOptions = [
	{ name: "register", alias: "r", type: Boolean },
	{ name: "registration-file", alias: "f", type: String },
	{ name: "config", alias: "c", type: String },
	{ name: "help", alias: "h", type: Boolean },
];
const options = Object.assign({
	"register": false,
	"registration-file": "email-registration.yaml",
	"config": "config.yaml",
	"help": false,
}, commandLineArgs(commandOptions));

if (options.help) {
	// tslint:disable-next-line:no-console
	console.log(commandLineUsage([
		{
			header: "famedly-email-bridge",
			content: "A bridge to bridge emails to matrix",
		},
		{
			header: "Options",
			optionList: commandOptions,
		},
	]));
	process.exit(0);
}

/*
 * Read the config file and apply custom logging
 */
function readConfig(): EmailConfig {
	const config = new EmailConfig();
	try {
		config.applyConfig(yaml.safeLoad(fs.readFileSync(options.config, "utf8")));
		Log.Configure(config.logging);

		// log matrix-bot-sdk with our logging
		const logMap = new Map<string, Log>();
		// tslint:disable-next-line:no-any
		const logFunc = (level: string, module: string, args: any[]) => {
			if (!Array.isArray(args)) {
				args = [args];
			}
			// filter out spam logging
			if (args.find((s) => s.includes && s.includes("M_USER_IN_USE"))) {
				return;
			}
			let mod = "bot-sdk-" + module;
			const modParts = module.match(/^(\S+)\s(.*)/);
			if (modParts) {
				const MOD_PARTS_NORMAL = 1;
				const MOD_PARTS_EXTRA = 2;
				if (modParts[MOD_PARTS_EXTRA]) {
					args.unshift(modParts[MOD_PARTS_EXTRA]);
				}
				mod = "bot-sdk-" + modParts[MOD_PARTS_NORMAL];
			}
			let logger = logMap.get(mod);
			if (!logger) {
				logger = new Log(mod);
				logMap.set(mod, logger);
			}
			logger[level](...args);
		};
		LogService.setLogger({
			// tslint:disable-next-line:no-any
			debug: (mod: string, args: any[]) => logFunc("debug", mod, args),
			// tslint:disable-next-line:no-any
			info: (mod: string, args: any[]) => logFunc("info", mod, args),
			// tslint:disable-next-line:no-any
			warn: (mod: string, args: any[]) => logFunc("warn", mod, args),
			// tslint:disable-next-line:no-any
			error: (mod: string, args: any[]) => logFunc("error", mod, args),
		});
	} catch (err) {
		log.error("Failed to read the config file", err);
		process.exit(-1);
	}
	return config;
}

/*
 * Generate the registartion file needed for synapse
 */
function generateRegistration(config: EmailConfig): void {
	log.info("Generating registration file...");
	if (fs.existsSync(options["registration-file"])) {
		log.error("Registration file already exists!");
		process.exit(-1);
	}
	const reg = {
		as_token: uuid(),
		hs_token: uuid(),
		id: "famedly-email-bridge",
		namespaces: {
			users: [{
				exclusive: true,
				regex: "@_email_.*",
			}],
			rooms: [ ],
			aliases: [{
				exclusive: true,
				regex: "#_email_.*",
			}],
		},
		protocols: [ ],
		rate_limit: false,
		sender_localpart: "_email",
		url: `http://${config.bridge.bindAddress}:${config.bridge.port}`,
	} as IAppserviceRegistration;
	fs.writeFileSync(options["registration-file"], yaml.safeDump(reg));
}

if (options.register) {
	generateRegistration(readConfig());
	process.exit(0);
}

async function run() {
	const config = readConfig();
	const bridge = new Bridge(options["registration-file"], config);
	await bridge.init();
	await bridge.start();
}

// tslint:disable-next-line:no-floating-promises
run(); // start the thing!
